import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MustBeLoggedGuard } from './core/guards/must-be-logged.guard';

const routes: Routes = [
  {
    path: 'dashboard',
    canActivate: [MustBeLoggedGuard],
    canLoad: [MustBeLoggedGuard],
    loadChildren: () =>
      import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
